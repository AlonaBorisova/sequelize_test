'use strict';

const Controller = require('./../../utils/controller');
const Models = require('./../../models/v1');
const async = require('async');

class AuthorsController extends Controller {
    constructor (version) {
        super(version);
        this.getList = [this.validator.authors.list, this.validateLimits, this._getList];
    }

    /**
     * get list of authors with filters
     * @param req
     * @param res
     * @param next
     * @private
     */
    _getList (req, res, next) {
        async.parallel({
            authors: function (callback) {
                Models.authors.findAll({
                        limit: req.query.limit,
                        offset: req.query.offset,
                        order: [['fullName', 'ASC']],
                        include: [
                            {
                                model: Models.books,
                                as: 'book_with_all_authors',
                                attributes:  ['title', 'cover_image'],
                                through: {
                                    attributes: []
                                }
                            }]})
                    .then(function (result) {
                        callback(null, result);
                    })
                    .catch(next);
            },
            total: function (callback) {
                Models.authors.count().then(function (count) {
                    callback(null, count);
                }).catch(callback);
            }
        }, function (err, result) {
            if (err) {
                return next(err);
            }

            res.send(result);
        });
    }
}

module.exports = AuthorsController;
