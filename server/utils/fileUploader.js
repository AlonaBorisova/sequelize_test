'use strict'
var async = require('async');
var easyimg = require('easyimage');
var md5 = require('md5');

//utils
var config = require('./../../config');

class FileUpload {

    imageUpload(file, size, callback) {
        var side;
        var newPhotoName = '';
        var newPhotoPath = '';

        var tmpFilePath = process.cwd() + '/frontend/app/image/';


        var photo = file.cover;


        if (config.uploadFiles.supportImagesTypes.indexOf(photo.type) === -1) {
            let err = new Error();
            err.status = 400;
            err.message = 'no valid file type!';
            return callback(err);
        }

        easyimg.info(photo.path)
            .then(function (file) {
                if (file.width < file.height) {
                    side = file.width;
                } else {
                    side = file.height;
                }
                //create new file name
                newPhotoName = md5(new Date().getTime()) + file.name;
                newPhotoPath = tmpFilePath + newPhotoName;

                easyimg.rescrop({
                    src: photo.path,
                    dst: newPhotoPath,
                    width: file.width,
                    height: file.height,
                    cropwidth: side,
                    cropheight: side,
                    gravity: 'center'
                })
                    .then(function (image) {
                        return callback(null, newPhotoName);
                    })
                    .catch(function (err) {
                        callback(err);
                    })
            })
            .catch(function (err) {
                callback(err);
            })
    }
}

module.exports = new FileUpload();