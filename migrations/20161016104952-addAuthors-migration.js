
module.exports = {
  up: function (queryInterface, sequelize,done) {
    queryInterface.sequelize.query("INSERT INTO authors (full_name, created_at, updated_at) VALUES('John Dow', NOW(), NOW())");
    done();
    // return [
    //   queryInterface.bulkInsert('authors', [
    //     { full_name: 'John Dow', created_at: Date.now(), updated_at: Date.now() },
    //     { full_name: 'Johnnes Dower', created_at: Date.now(), updated_at: Date.now() },
    //     { full_name: 'Billy Bonn', created_at: Date.now(), updated_at: Date.now() },
    //     { full_name: 'Stew Ronn', created_at: Date.now(), updated_at: Date.now() },
    //     { full_name: 'Harry Toll', created_at: Date.now(), updated_at: Date.now() },
    //     { full_name: 'Willy Domm', created_at: Date.now(), updated_at: Date.now() }
    //   ])
    //
    // ];
  },

  down: function (queryInterface, Sequelize) {

  }
};

// INSERT INTO genres (genre_name, created_at, updated_at) VALUES('Mystery', NOW(), NOW());
// INSERT INTO genres (genre_name, created_at, updated_at) VALUES('Science', NOW(), NOW());
// INSERT INTO genres (genre_name, created_at, updated_at) VALUES('Fantasy', NOW(), NOW());
// INSERT INTO genres (genre_name, created_at, updated_at) VALUES('History', NOW(), NOW());
// INSERT INTO genres (genre_name, created_at, updated_at) VALUES('Health', NOW(), NOW());
//
//
//
// INSERT INTO authors (full_name, created_at, updated_at) VALUES('John Dow', NOW(), NOW());
// INSERT INTO authors (full_name, created_at, updated_at) VALUES('Johnnes Dower', NOW(), NOW());
// INSERT INTO authors (full_name, created_at, updated_at) VALUES('Billy Bonn', NOW(), NOW());
// INSERT INTO authors (full_name, created_at, updated_at) VALUES('Stew Ronn', NOW(), NOW());
// INSERT INTO authors (full_name, created_at, updated_at) VALUES('Harry Toll', NOW(), NOW());
//
//
// INSERT INTO publishers (publisher_name, code, created_at, updated_at) VALUES('Planet', '027',NOW(), NOW());
// INSERT INTO publishers (publisher_name, code, created_at, updated_at) VALUES('Rainbow', '031', NOW(), NOW());
// INSERT INTO publishers (publisher_name, code, created_at, updated_at) VALUES('Music', '026', NOW(), NOW());
// INSERT INTO publishers (publisher_name, code, created_at, updated_at) VALUES('World', '041', NOW(), NOW());
// INSERT INTO publishers (publisher_name, code, created_at, updated_at) VALUES('Baby', '102', NOW(), NOW());
//
// INSERT INTO books (title, cover_image, genre_id, publisher_id, isbn, created_at, updated_at) VALUES('At my Planet', 'https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwiaqpyE0NfPAhVH2CwKHVrfB-gQjRwIBw&url=http%3A%2F%2Fwww.freedigitalphotos.net%2F&psig=AFQjCNFXtgoT69-kJ64TGBhiyBYpnHf9PA&ust=1476442760899812', 2, 4,'027',NOW(), NOW());
// INSERT INTO books (title, cover_image, genre_id, publisher_id, isbn, created_at, updated_at) VALUES('Where is Rainbow?', 'https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwi4xPOS0NfPAhXGjSwKHVZuCngQjRwIBw&url=https%3A%2F%2Fpixabay.com%2Fen%2Fphotos%2Fcat%2F&psig=AFQjCNFXtgoT69-kJ64TGBhiyBYpnHf9PA&ust=1476442760899812',  3, 5,'031', NOW(), NOW());
// INSERT INTO books (title, cover_image, genre_id, publisher_id, isbn, created_at, updated_at) VALUES('Music is all about us', 'https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwi3tMyY0NfPAhVChSwKHYZKBv4QjRwIBw&url=https%3A%2F%2Fpixabay.com%2Fen%2Farchitecture-images-art-work-of-art-888133%2F&psig=AFQjCNFXtgoT69-kJ64TGBhiyBYpnHf9PA&ust=1476442760899812',1, 2,'026', NOW(), NOW());
// INSERT INTO books (title, cover_image, genre_id, publisher_id, isbn, created_at, updated_at) VALUES('The World is mine', 'https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjfro2g0NfPAhUJCSwKHXbcDA8QjRwIBw&url=http%3A%2F%2Fwww.bbc.com%2Fnews%2Fin-pictures-35744907&psig=AFQjCNFXtgoT69-kJ64TGBhiyBYpnHf9PA&ust=1476442760899812', 4, 3,'041', NOW(), NOW());
// INSERT INTO books (title, cover_image, genre_id, publisher_id, isbn, created_at, updated_at) VALUES('Baby, one more time', 'https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&cad=rja&uact=8&ved=0ahUKEwjl2Lqm0NfPAhVJDywKHStlD2AQjRwIBw&url=http%3A%2F%2Ftwistedsifter.com%2F2016%2F03%2Fpopular-images-with-a-twist-by-tony-futura%2F&psig=AFQjCNFXtgoT69-kJ64TGBhiyBYpnHf9PA&ust=1476442760899812',  5, 1,'102', NOW(), NOW());
//
//
// INSERT INTO book_author ( createdAt, updatedAt, author_id, book_id) VALUES( NOW(), NOW(), 1, 4);
// INSERT INTO book_author ( createdAt, updatedAt,  author_id, book_id) VALUES( NOW(), NOW(), 2, 3);
// INSERT INTO book_author (  createdAt, updatedAt,  author_id, book_id) VALUES( NOW(), NOW(), 3, 2);
// INSERT INTO book_author (  createdAt, updatedAt,  author_id, book_id) VALUES( NOW(), NOW(), 4, 1);
// INSERT INTO book_author (  createdAt, updatedAt,  author_id, book_id) VALUES(NOW(), NOW(), 5, 5);
// INSERT INTO book_author (  createdAt, updatedAt, author_id, book_id) VALUES( NOW(), NOW(), 3, 1);
// INSERT INTO book_author (  createdAt, updatedAt,  author_id, book_id) VALUES(NOW(), NOW(), 4, 2 );
// INSERT INTO book_author ( createdAt, updatedAt, author_id, book_id) VALUES( NOW(), NOW(), 1, 6);
// INSERT INTO book_author ( createdAt, updatedAt, author_id, book_id) VALUES( NOW(), NOW(), 2, 6);
// INSERT INTO book_author ( createdAt, updatedAt, author_id, book_id) VALUES( NOW(), NOW(), 3, 6);
// INSERT INTO book_author ( createdAt, updatedAt, author_id, book_id) VALUES( NOW(), NOW(), 4, 6);
//
//
// use books;
//
// --  delete from genres where id=1;
// --   delete from genres where id=2;
// --    delete from genres where id=3;
// --     delete from genres where id=4;
// --      delete from genres where id=5;
//
//
// --
//     -- UPDATE books SET cover_image='http://www.gettyimages.pt/gi-resources/images/Homepage/Hero/PT/PT_hero_42_153645159.jpg'
// --  where id=6;
// --  UPDATE books SET cover_image='http://www.freedigitalphotos.net/images/img/homepage/87357.jpg'
// --  where id=5;
// --  UPDATE books SET cover_image='http://www.planwallpaper.com/static/images/stunning-images-of-the-space.jpg'
// --  where id=4;
// --  UPDATE books SET cover_image='http://www.apimages.com/Images/AP641354276908.jpg'
// --  where id=3;
// --  UPDATE books SET cover_image='http://i.dailymail.co.uk/i/pix/2016/04/13/00/331D901800000578-3536787-image-a-11_1460503122350.jpg'
// --  where id=2;
// --  UPDATE books SET cover_image='http://www.telegraph.co.uk/content/dam/technology/2016/02/05/Jennifer-in-Paradi_3204219a-large_trans++qVzuuqpFlyLIwiB6NTmJwfSVWeZ_vEN7c6bHu2jJnT8.jpg'
// --  where id=1;
//
//
// --
//
//     --  UPDATE book_author SET author_id=3
// --  where book_id=6;
// --  UPDATE book_author SET author_id=2
// --  where book_id=6;
// --  UPDATE book_author SET author_id=4
// --  where book_id=6;
// select * from book_author;
// -- drop table book_author;
// -- drop table authors;
// -- drop table books;
// -- drop table genres;
// -- drop table publishers;

