'use strict';

module.exports = function () {
    function base(data) {
        function userStructure(item) {
            return {
                book_id: item.book_id,
                author_id: item.author_id
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (user) {
                return userStructure(user);
            });

            return result;
        }

        return userStructure(data);
    }

    return {
        base: base
    }
};

