'use strict';

const config = require('./config');
const app = require('./server');
const http = require('http');

const serverStartCallback = function () {
    console.log('info', 'Web server successfully started at port ', config.server.port);
};


let server = http.createServer(app)
    .listen(config.server.port, serverStartCallback);



