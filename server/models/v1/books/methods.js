'use strict';

module.exports = {
    classMethods: {
        associate: function (models) {
            this.belongsTo(models.publishers, {
                foreignKey: {
                    name: 'publisher_id',
                    allowNull: false
                }
            });
            this.belongsTo(models.genres, {
                foreignKey: {
                    name: 'genre_id',
                    allowNull: false
                }
            });
            this.belongsToMany(models.authors, {
                as: 'authors_in_book',
                through:  {
                    model: models.book_author
                },
                foreignKey: 'book_id',
                otherKey: 'author_id'
            });
        },
        COVER_SIZES: function () {
            return [{
                    width: 128,
                    height: 128
                }
            ]
        }
    },
    scopes: {
        defaultAttributesSet: {
            attributes:['id', 'title', 'cover_image', 'ISBN', 'publisher_id', 'genre_id','created_at', 'updated_at']
        }
    }
};
