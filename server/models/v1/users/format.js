'use strict';

module.exports = function () {
    function base(data) {
        function userStructure(item) {
            item = item.dataValues;
            return {
                id: item.id,
                email: item.email || '',
                user_name: item.user_name,
                avatar: item.avatar || '',
                bitbucket_id: item.bitbucket_id || '',
                createdAt: item.createdAt,
                updatedAt: item.updatedAt,
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (user) {
                return userStructure(user);
            });

            return result;
        }

        return userStructure(data);
    }

    return {
        base: base
    }
};
