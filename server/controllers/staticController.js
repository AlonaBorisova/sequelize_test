var fs = require('fs');

module.exports = {
    index: function (req, res, next) {
        fs.createReadStream(__dirname + '/../../frontend/app/index.html')
            .pipe(res);
    }
};
