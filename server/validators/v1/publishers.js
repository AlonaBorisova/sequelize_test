'use strict';
var Base = require('./../../utils/baseValidator');
var validator = require('./../../utils/bodyValidator');

class PublishersValidator extends Base{

    static list (req, res, next){
        return validator({
            properties: {
                offset: {
                    type: 'any',
                    allowEmpty: true,
                    required: false
                },
                limit: {
                    type: 'any',
                    allowEmpty: true,
                    required: false
                },
                order_field: {
                    type: 'string',
                    allowEmpty: false,
                    required: false
                },
                order_type: {
                    type: 'string',
                    allowEmpty: false,
                    required: false,
                    enum: ['asc', 'desc', 'ASC', 'DESC']
                }
            }
        })(req, res, next);
    };
}
module.exports = PublishersValidator;
