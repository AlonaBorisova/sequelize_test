'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        user_name: {
            type: DataTypes.STRING,
            field: 'user_name'
        },
        email: {
            type: DataTypes.STRING,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING
        },
        avatar: {
            type: DataTypes.STRING,
            field: 'avatar'
        },
        bitbucket_id: {
            type: DataTypes.STRING,
            field: 'bitbacket_id'
        },
        created_at: {
            type: DataTypes.DATE,
            field: 'created_at'
        },
        updated_at: {
            type: DataTypes.DATE,
            field: 'updated_at'
        }
    }
};
