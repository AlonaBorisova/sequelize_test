'use strict';

const Controller = require('./../../utils/controller');
const Models = require('./../../models/v1');
const Upload = require('./../../utils/fileUploader');
const async = require('async');

class BooksController extends Controller {
    constructor (version) {
        super(version);

        this.getById = [this._getBookById];
        this.getList = [this.validator.books.list, this.validateLimits, this._getList];
        this.addBook = [this.validator.books.add, this._uploadCover,  this._addBook];
        this.deleteBook = [this._delete];
    }


    /**
     * return book data by id
     * @param req
     * @param res
     * @param next
     * @private
     */
    _getBookById (req, res, next) {
        Models.books.findById(req.params.id)
            .then(function (book) {
                if (!book) {
                    let error = new Error();
                    error.message = "book not found";
                    error.status = 404;
                    return next(error);
                }

                res.send(Models.books.format.base(book));
            })
            .catch(next);
    }

    /**
     * get list of book
     * @param req
     * @param res
     * @param next
     * @private
     */
    _getList (req, res, next) {
        async.parallel({
            books: function (callback) {
                Models.books.scope('defaultAttributesSet')
                    .findAll({
                        limit: req.query.limit,
                        offset: req.query.offset,
                        order: [['title', 'ASC']],

                        include: [
                            {
                                model: Models.authors,
                                as: 'authors_in_book',
                                subQuery: false,
                                required: false,
                                attributes: ['fullName'],
                                through: {
                                    required: false,
                                    attributes: []
                                }
                            },
                            {
                                model: Models.publishers,
                                attributes: ['publisher_name'],
                                required: false,
                            },
                            {
                                model: Models.genres,
                                attributes: ['genre_name'],
                                required: false,
                            },

                        ],

                    })
                    .then(function (result) {
                        callback(null, Models.books.format.base(result));
                    })
                    .catch(next);
            },
            total: function (callback) {
                Models.books.count().then(function (count) {
                    callback(null, count);
                })
                    .catch(callback);
            }
        }, function (err, result) {
            if (err) {
                return next(err);
            }

            res.send(result);
        });
    }

    /**
     * Upload new book cover
     * @param req
     * @param res
     * @param next
     * @private
     */

    _uploadCover(req, res, next) {
        if (!req.files || !req.files.cover) {
            req.local.cover = null;
            return next();
        }

        Upload.imageUpload(req.files, Models.books.COVER_SIZES()[0], function (err, fileName) {
            if (err) {
                return next(err);
            }

            req.local = fileName;
            next();
        });
    }
    /**
     * add new book
     * @param req
     * @param res
     * @param next
     * @private
     */
    _addBook (req, res, next) {
        var params = {
            title: req.body.title,
            cover_image: req.local,
            ISBN: req.body.isbn,
            publisher_id: Number(req.body.publisher_id),
            genre_id:  Number(req.body.genre_id)

        };
        var authors = req.body.authors.split(',');

        Models.books.create(params)
            .then(function (book) {
                if(authors.length > 0) {
                    async.each(authors, function (author, callback) {
                        return Models.authors.sequelize.transaction(function (t) {
                            return Models.book_author.create( {
                                book_id: book.id,
                                author_id: Number(author),
                            }, {
                                transaction: t
                            });
                        }).then(function (result) {
                            callback(null, result)
                        }).catch(function (err) {
                            return callback(err);
                        })
                    }, function done(err, callback) {
                        if (err) {
                            return callback(err);
                        }

                        res.status(201).send()
                    });
                }
            }).catch(next);
    }


    /**
     * delete book
     * @param req
     * @param res
     * @param next
     * @private
     */
    _delete (req, res, next) {
        Models.books.destroy({
            where: {
                id: req.params.id
            }
        }).then(function () {
            Models.book_author.destroy({
                where: {
                    book_id: req.params.id
                }
            }).then(function () {
                res.status(204);
                res.send();
            });
        }).catch(next);
    }

    /**
     * check if book exists
     * @param req
     * @param res
     * @param next
     * @private
     */
    _checkBook (req, res, next) {
        Models.book.findById(req.params.id)
            .then(function (data) {
                if (!data) {
                    let error = new Error();
                    error.message = "book does not exist";
                    error.status = 404;
                    return next(error);
                }

                res.locals.book = data;
                next();
            })
            .catch(next);
    }
}

module.exports = BooksController;
