'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('./utils/errorHandler');
const disallowMethods = require('./utils/methodsHandler');
const requestHandler = require('./utils/requestHandler');
const router = require('./routes');
const passport = require('passport');
const controllers = require('./controllers');

//initialize the app
const app = module.exports = express();
const session = require('express-session');

//set up static files directory
app.use(express.static(__dirname + '/../frontend/app'));
app.use(requestHandler);
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.raw({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({ secret: 'hohoho',
    cookie: { secure: false },
    resave: false,
    saveUninitialized: true}));

app.use(passport.initialize());
//app.use(passport.session());

app.use(router);

//set up http error handler
app.use(errorHandler(app));

disallowMethods(app);

process.on('uncaughtException', function (err) {
    console.error((new Date).toUTCString() + ' uncaughtException:', err.message);
    console.error(err.stack);
    process.exit(1);
});



