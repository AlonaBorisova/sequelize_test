/**
 * INSPINIA - Responsive Admin Theme
 *
 */

/**
 * MainCtrl - controller
 */
angular.module('inspinia').controller('MainCtrl', [ '$rootScope', '$scope', '$location', '$http', function ($rootScope, $scope, $location, $http) {
    $scope.helloText = 'Welcome to Book Shelf';
    $scope.descriptionText = 'This is the "Book Shelf" admin page';

    var adminToken = sessionStorage.getItem('adminToken'); // secured
    if (!adminToken) {
        $location.path('/login');
    } else {
        // here should be admin token checking
        $http.defaults.headers.common.Authorization = 'admin ' + adminToken;
    }

    $scope.logout = function () {
        sessionStorage.removeItem('adminToken');
        $location.path('/login');
    };
}]);

angular.module('inspinia').controller('LoginCtrl', ['$scope', '$http', '$location', 'fetchService',
    function ($scope, $http, $location, fetchService) {
    var token = sessionStorage.getItem('adminToken');
    if (token) {
        // checking token here
        $location.path('/index.main');
    }
    $scope.loginUser = function () {
        if ($scope.login && $scope.password) {
            fetchService.loginUser($scope.login, $scope.password)
                .then(function(res){
                    if (res.accessToken) {
                        sessionStorage.setItem('adminToken', res.accessToken);
                        $http.defaults.headers.common.Authorization = res.accessToken;
                        $location.path('/index.main');
                    }
                    $location.path('/index.main');
                }, function(data){
                    console.log('Wrong credentials', data);
                });
        }
    };
}]);

angular.module('inspinia').controller('AddBookCtrl', ['$scope', '$http',
    'fetchService', '_',
    function ($scope, $http, fetchService, _) {
        $scope.book = {};
        $scope.selectedAuthors = [];
        $scope.selectedIds = [];
        var params = {
            limit: 20,
            offset: 0
        };
        var handleFileSelect=function(evt) {
            var file=evt.currentTarget.files[0];
            var reader = new FileReader();
            reader.onload = function (evt) {
                $scope.$apply(function($scope){
                    $scope.myImage = evt.target.result;

                    $scope.book.cover = file;
                    console.log($scope.book.cover)
                });
            };
            reader.readAsDataURL(file);

        };

        angular.element(document.querySelector('#inputImage')).on('change',handleFileSelect);

         function getPublishers() {
            fetchService.loadPublishers(params)
                .then(function(res){
                    $scope.publishers = res.publishers;
                }, function(data){
                    console.log('error', data);
                });
        }

        function getAuthors(limit, offset) {
            var params = {
                limit: limit,
                offset: offset
            };

            fetchService.loadAuthors(params)
                .then(function(res){
                    $scope.authors = res.authors;
                    $scope.totalItems = res.total;
                }, function(res){
                    console.log(res);
                });
        }

        function getGenres() {
            fetchService.loadGenres(params)
                .then(function(res){
                    $scope.genres = res.genres;
                }, function(data){
                        console.log('error', data);
                });
        }

        $scope.init = function () {
            getGenres();
            getPublishers();
            getAuthors();
        };
        $scope.init();

        $scope.chandeAuthor = function (genreId) {
            var selectedGenre = _.find($scope.authors, function (genre) {
                return genre.id === +(genreId[0]);
            });

            if(_.contains($scope.selectedAuthors, selectedGenre.fullName)) {
                var index = _.indexOf($scope.selectedAuthors, selectedGenre.fullName);
                if (index >= 0) {
                    $scope.selectedAuthors.splice( index, 1 );
                    $scope.selectedIds.splice( index, 1);
                }
            } else {
                $scope.selectedAuthors.push(selectedGenre.fullName);
                $scope.selectedIds.push(selectedGenre.id);
            }
        };

        $scope.submitForm = function () {
        if ($scope.book_form.$valid && !$scope.book_form.publisher.$invalid && !$scope.book_form.genre.$invalid) {

           // $scope.book.cover = $scope.myImage ? $scope.myImage : 'http://photos.gograph.com/thumbs/CSP/CSP570/k26855868.jpg';
            console.log($scope.book);
            var fd = new FormData();
            fd.append('cover', $scope.book.cover);
            fd.append('title', $scope.book.title);
            fd.append('publisher_id', $scope.book.publisher_id);
            fd.append('genre_id', $scope.book.genre_id);
            fd.append('isbn', $scope.book.isbn);
            fd.append('authors', $scope.selectedIds)
            fetchService.addBook(fd).then(function (result) {
                console.log(result);
                $scope.$close();
            }, function (err) {
                console.log(err);
            });
        } else {
            $scope.book_form.submitted = true;
            $scope.book_form.publisher.$invalid = ($scope.book.publisher_id === -1);
            $scope.book_form.genre.$invalid = ($scope.book.genre_id === -1);
        }
        };
    }]);

angular.module('inspinia').controller('AuthorsCtrl', ['$scope', '$http', '$modal', 'fetchService',
    function ($scope, $http, $modal, fetchService) {
         $scope.currentPage = {
            page: 1,
            limit: 10
         };

         function getAuthors(limit, offset) {
            var params = {
                limit: limit,
                offset: offset
            };

            fetchService.loadAuthors(params)
                .then(function(res){
                    $scope.authors = res.authors;
                    $scope.totalItems = res.total;
                }, function(res){
                    console.log(res);
                });
        }

         $scope.pageChanged = function() {
            // shift offset for correct data fetching
            $scope.offset = ($scope.currentPage.page - 1)*$scope.currentPage.limit;
             getAuthors($scope.currentPage.limit, $scope.offset);
        };


        $scope.removeAuthor = function (event) {
            var modalInstance = $modal.open({
                templateUrl: 'views/warningDelete.html',
                windowClass: "animated",
                controller: 'ModalWarningEventCtrl',
                resolve: {
                    eventInfo: function () {
                        return event;
                    }
                }
            });
            modalInstance.result.then(function() {
                $scope.init();
            }, function() {
                console.log('Cancelled');
            })['finally'](function(){
                modalInstance = undefined
            });
        };

        $scope.openAddEventModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/addBook.html',
                windowClass: "animated",
                controller: 'AddEventCtrl'
            });

            modalInstance.result.then(function() {
                $scope.currentPage.page = 1;
                $scope.pageChanged();
            }, function() {
                console.log('Cancelled');
            })['finally'](function(){
                modalInstance = undefined
            });
        };


        $scope.init = function () {
            $scope.pageChanged();
        };
        $scope.init();
    }]);

angular.module('inspinia').controller('BooksCtrl', ['$scope', '$http', '$modal', 'fetchService', '_',
    function ($scope, $http, $modal, fetchService, _) {
        $scope.currentPage = {
            page: 1,
            limit: 10
        };

        function getBooks(limit, offset) {
            var params = {
                limit: limit,
                offset: offset
            };

            fetchService.loadBooks(params)
                .then(function(res){
                    $scope.books = res.books;
                    $scope.totalItems = res.total;
                }, function(res){
                    console.log(res);
                });
        }

        // $scope.removeBook = function (book) {
        //     var modalInstance = $modal.open({
        //         templateUrl: 'views/warningDelete.html',
        //         windowClass: "animated",
        //         controller: 'ModalWarningCtrl',
        //         resolve: {
        //             bookInfo: function () {
        //                 return book;
        //             }
        //         }
        //     });
        //     modalInstance.result.then(function() {
        //         $scope.init();
        //     }, function() {
        //         console.log('Cancelled');
        //     })['finally'](function(){
        //         modalInstance = undefined
        //     });
        // };

        $scope.removeBook = function (book) {
            var book = _.findWhere($scope.books, {
                id: book.id
            });
            if (book) {
                fetchService.deleteBook(book.id)
                    .then(function (res) {
                        console.log('book deleted');
                        $scope.books = _.without($scope.books, book);
                    }, function (data) {
                        console.log('error', data);
                    });
            }
        };

        $scope.pageChanged = function() {
            // shift offset for correct data fetching
            $scope.offset = ($scope.currentPage.page - 1)*$scope.currentPage.limit;
            getBooks($scope.currentPage.limit, $scope.offset);
        };

        $scope.openAddBookModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/addBook.html',
                windowClass: "animated",
                controller: 'AddBookCtrl'
            });

            modalInstance.result.then(function() {
                $scope.currentPage.page = 1;
                $scope.pageChanged();
            }, function() {
                console.log('Cancelled');
            })['finally'](function(){
                modalInstance = undefined
            });
        };


        $scope.init = function () {
            $scope.pageChanged();
        };
        $scope.init();
    }]);

angular.module('inspinia').controller('ModalWarningCtrl', ['$scope',  '$http', 'fetchService', 'bookInfo',
    function ($scope, $http, fetchService, bookInfo) {
        $scope.title = bookInfo.title;

        $scope.removeEvent = function () {
            fetchService.deleteBook(bookInfo._id)
                .then(function(res){
                    $scope.$close();
                }, function(res){
                    console.log(res);
                });
        };
    }]);


