'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        name: {
            type: DataTypes.STRING,
            field: 'publisher_name',
            allowNull: false,
            validate: {
                max: 100,
                min: 5
            },
        },
        code: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        created_at: {
            type: DataTypes.DATE,
            field: 'created_at',
            defaultValue: DataTypes.NOW
        },
        updated_at: {
            type: DataTypes.DATE,
            field: 'updated_at',
            defaultValue: DataTypes.NOW

        }
    }
};
