const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart();
// use passport
const auth = require('../helpers');
const controllers = require('../controllers');
const config = require('../../config');
// initialize  passport
auth.local.init();


 router.post('/api/v1/login', passport.authenticate('local',{session: false , failureRedirect: '/noway'}),
function (req, res, next) {
    var session = {};
    //start admin session
    let tokenParams = {
        createTime: Date.now(),
        id: req.user.dataValues.id
    };
    session.accessToken = jwt.sign(tokenParams, config.jwt.jwtKey);
    session.tokenExpiresAt = new Date(tokenParams.createTime + config.jwt.jwtLifeTime).toISOString();
    session.userName = req.user.dataValues.user_name;

    res.send(session);
});


router.route('/api/:version/authors')
    .get(controllers.callAction('authors.getList'));

router.route('/api/:version/books')
    .get(controllers.callAction('books.getList'))
    .post(multipartMiddleware, controllers.callAction('books.addBook'));
router.route('/api/:version/books/:id')
    .get(controllers.callAction('books.getById'))
    .delete(controllers.callAction('books.deleteBook'));

router.route('/api/:version/publishers')
    .get(controllers.callAction('publishers.getList'));

router.route('/api/:version/genres')
    .get(controllers.callAction('genres.getList'));

module.exports = router;