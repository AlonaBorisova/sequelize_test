'use strict';

module.exports = function () {
    function base(data) {
        function userStructure(item) {
            item = item.dataValues;
            return {
                id: item.id,
                name: item.name,
                code: item.code
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (user) {
                return userStructure(user);
            });

            return result;
        }

        return userStructure(data);
    }

    return {
        base: base
    }
};

