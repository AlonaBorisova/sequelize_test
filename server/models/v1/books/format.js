'use strict';

module.exports = function () {
    function base(data) {
        function bookStructure(item) {
            item = item.dataValues;
            //item.cover_image = 'app/image/' + item.cover_image;
            return {
                id: item.id,
                title: item.title,
                coverImage: item.cover_image,
                ISBN: item.ISBN,
                createdAt: item.created_at,
                updatedAt: item.updated_at,
                publisher: item.publisher.dataValues.publisher_name,
                genre: item.genre.dataValues.genre_name,
                AuthorsInBook: item.authors_in_book,
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            return data.map(function (book) {
                return bookStructure(book);
            });
        }

        return userStructure(data);
    }

    return {
        base: base
    };
};

