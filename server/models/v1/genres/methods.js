'use strict';

module.exports = {
    classMethods: {
        associate: function (models) {
            this.hasMany(models.books, {
                foreignKey: {
                    name: 'genre_id',
                    allowNull: false
                },
                as: 'books_variety'
            });
        }
    },
    createdAt:false,
    updatedAt:false
};
