"use strict";

module.exports = {
    classMethods: {
        associate: function (models) {
            this.hasMany(models.books, {
                foreignKey: {
                    name: 'publisher_id',
                    allowNull: false
                }
            });
        },
    },
    createdAt:false,
    updatedAt:false
};
