"use strict";

module.exports = {
    classMethods: {
        associate: function (models) {
            this.belongsToMany(models.books, {
                as: 'book_with_all_authors',
                through: {
                    model: models.book_author
                },
                foreignKey: 'author_id',
                otherKey: 'book_id'
            });
        }
    }
};
