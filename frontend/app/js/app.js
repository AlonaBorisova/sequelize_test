/**
 * INSPINIA - Responsive Admin Theme
 *
 */
(function () {
    var underscore = angular.module('underscore', []);
    underscore.factory('_', function() {
        return window._; //Underscore must already be loaded on the page
    });
    
    angular.module('inspinia', [
        'ui.router',                    // Routing
        'ui.bootstrap',
        'underscore'// Ui Bootstrap
    ])
})();

