'use strict';

module.exports = function () {
    function base(data) {
        function userStructure(item) {
            return {
                id: item.id,
                name: item.name
            }
        }

        // when data - array with models
        if (Array.isArray(data)) {
            let result = data.map(function (user) {
                return userStructure(user);
            });

            return result;
        }

        return userStructure(data);
    }

    return {
        base: base
    }
};

