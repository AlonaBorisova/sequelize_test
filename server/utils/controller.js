'use strict';
var jwt = require('jsonwebtoken');
var config = require('./../../config');

class Controller {
    constructor (version) {
        this.VERSION = version;

        /**
         * validate skip and count parameters for getting any resource
         * @param req
         * @param res
         * @param next
         * @returns {*}
         * @private
         */
        this.validateLimits  = function (req, res, next) {
            req.query.limit = parseInt(req.query.limit) ? parseInt(req.query.limit) : 50;
            req.query.offset = parseInt(req.query.offset) ? parseInt(req.query.offset) : 0;

            return next();
        };

        this.validator = require('./../validators/' + version)
    }

    /**
     * Create token
     * @private
     */
    static _createUserToken(client) {
        let tokenParams = {};
        tokenParams.createTime = Date.now();
        tokenParams.id = client.id;

        let user = client.dataValues || client;
        user.token = jwt.sign(tokenParams, config.jwt.jwtKey);
        user.tokenExpiresAt = new Date(tokenParams.createTime + config.jwt.jwtLifeTime).toISOString();
        return user;
    }
}

module.exports = Controller;
