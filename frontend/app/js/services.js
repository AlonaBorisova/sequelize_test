angular.module('inspinia').service('fetchService', [ '$http', '$q',
    function($http, $q){

        var loginUser = function(login, password){
            var deferred =$q.defer();

            $http({
                url: '/api/v1/login',
                method: 'POST',
                data: {
                    usernameField: login,
                    passwordField: password
                }
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };
        
        var addBook = function (params) {
            var deferred =$q.defer();
            $http({
                url: '/api/v1/books',
                method: 'POST',
                headers: {'Content-Type': undefined},
                data: params
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };
        
        var deleteBook = function (bookId) {
            var deferred =$q.defer();
            $http({
                url: '/api/v1/books/' + bookId,
                method: 'DELETE'
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var loadGenres = function (params) {
            var deferred =$q.defer();

            $http({
                url: '/api/v1/genres?limit='  + params.limit + '&offset='+ params.offset,
                method: 'GET',
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var loadPublishers = function (params) {
            var deferred =$q.defer();

            $http({
                url: '/api/v1/publishers?limit='  + params.limit + '&offset='+ params.offset,
                method: 'GET',
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var loadAuthors = function (params) {
            var deferred =$q.defer();
            
            $http({
                url: '/api/v1/authors?limit='  + params.limit + '&offset='+ params.offset,
                method: 'GET',
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var getBookById = function (bookId) {
            var deferred =$q.defer();

            $http({
                url: '/api/v1/books/'  + bookId,
                method: 'GET',
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var loadBooks = function (params) {
            var deferred =$q.defer();

            $http({
                url: '/api/v1/books?limit='  + params.limit + '&offset='+ params.offset,
                method: 'GET',
            }).success(function(response) {
                deferred.resolve(response);
            }).error(function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        return {
            loginUser: loginUser,
            addBook: addBook,
            loadGenres: loadGenres,
            loadPublishers: loadPublishers,
            deleteBook: deleteBook,
            getBookById: getBookById,
            loadAuthors: loadAuthors,
            loadBooks: loadBooks
        }
}]);
