'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        title: {
            type: DataTypes.STRING,
            field: 'title',
            allowNull: false,
            validate: {
                max: 100,
                min: 2
            },
        },
        cover_image: {
            type: DataTypes.STRING,
            allowNull: true,
            field: 'cover_image',
        },
        // genre_id: {
        //     type: DataTypes.INTEGER,
        //     allowNull: true,
        //     field: 'genre_id',
        // },
        // publisher_id: {
        //     type: DataTypes.INTEGER,
        //     allowNull: true,
        //     field: 'publisher_id',
        // },
        ISBN: {
            type: DataTypes.STRING,
            field: 'isbn',
            allowNull: false,
            validate: {
                //max: 13,
                min: 9,
                //pattern: /^[0-9]+$/
            },
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at',
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at',
            defaultValue: DataTypes.NOW

        }
    }
};
