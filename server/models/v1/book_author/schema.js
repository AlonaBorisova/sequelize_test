'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        book_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'book_id',
        },
        author_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'author_id',
        },
    }
};
