'use strict';

const Controller = require('./../../utils/controller');
const Models = require('./../../models/v1');

class PublishersController extends Controller {
    constructor (version) {
        super(version);

        this.getList = [this.validator.authors.list, this.validateLimits, this._buildFilters, this._getList];
    }

    /**
     * build filters for getting users
     * @param req
     * @param res
     * @param next
     * @private
     */
    _buildFilters (req, res, next) {
        var config = {
            limit: req.query.limit,
            offset: req.query.offset,
        };

        config.order = [['publisher_name', 'ASC']];

        if (req.query.order_field) {
            req.query.order_type = req.query.order_type || 'ASC';
            config.order = [[req.query.order_field, req.query.order_type]]
        }

        res.locals.config = config;
        next();
    }


    /**
     * get list of publishers with filters
     * @param req
     * @param res
     * @param next
     * @private
     */
    _getList (req, res, next) {
        var response = {total: 0, publishers: []};

        Models.publishers.findAndCountAll(res.locals.config)
            .then(function (result) {
                response.total = result.count;
                response.publishers = Models.publishers.format.base(result.rows);

                res.send(response);
            })
            .catch(next);
    }
}

module.exports = PublishersController;
