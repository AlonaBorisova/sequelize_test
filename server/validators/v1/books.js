'use strict';
var Base = require('./../../utils/baseValidator');
var validator = require('./../../utils/bodyValidator');

class BooksValidator extends Base{

    static list (req, res, next){
        return validator({
            properties: {
                offset: {
                    type: 'any',
                    allowEmpty: true,
                    required: false
                },
                limit: {
                    type: 'any',
                    allowEmpty: true,
                    required: false
                },
                order_field: {
                    type: 'string',
                    allowEmpty: false,
                    required: false
                },
                order_type: {
                    type: 'string',
                    allowEmpty: false,
                    required: false,
                    enum: ['asc', 'desc', 'ASC', 'DESC']
                }
            }
        })(req, res, next);
    };

    static add (req, res, next){
        return validator({
            properties: {
                title: {
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
                cover_image: {
                    type: 'string',
                    allowEmpty: true,
                },
                isbn: {
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
                publisher_id: {
                    type: 'any',
                    allowEmpty: true,
                    required: true
                },
                genres_id: {
                    type: 'any',
                    allowEmpty: true,
                    required: true
                }
            }
        })(req, res, next);
    };

    static edit (req, res, next){
        return validator({
            properties: {
                title: {
                    type: 'string',
                    allowEmpty: false,
                    required: true
                },
                cover_image: {
                    type: 'any',
                    allowEmpty: false,
                    required: true,
                    format: 'email'
                },
                password: {
                    type: 'any',
                    allowEmpty: false,
                    required: true
                }
            }
        })(req, res, next);
    };


}
module.exports = BooksValidator;
