'use strict';

module.exports = function(sequelize, DataTypes){
    return {
        fullName: {
            type: DataTypes.STRING,
            field: 'full_name',
            allowNull: false,
            validate: {
                max: 300,
                min: 2
            },
        },
        createdAt: {
            type: DataTypes.DATE,
            field: 'created_at',
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            field: 'updated_at',
            defaultValue: DataTypes.NOW

        }
    }
};
